using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour{
    
    [SerializeField]
    private Vector3 axes; //Scale axis
    public float scaleUnits; //Scale speed


    // Update is called once per frame
    void Update(){
        
        axes = MovementScript.ClampVector3(axes);

        // La escala es acumulativa
        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
}
