using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes; //Movement Axis
    public float speed; //Movement Speed

    // Update is called once per frame
    void Update()
    {
        axes = MovementScript.ClampVector3(axes);


        transform.Rotate(axes * (speed * Time.deltaTime));
    }
}
