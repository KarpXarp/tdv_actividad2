using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction; //Movement Axis
    public float speed; //Movement Speed

    // Update is called once per frame
    void Update(){

        //Valores de direccion
        direction = ClampVector3(direction);

        //Movimiento del componente transform
        transform.Translate(direction * (speed * Time.deltaTime));

    }

    public static Vector3 ClampVector3(Vector3 target){

        /*clamped hace q en el caso de q el valor supere alguno de los valores definidos se vuelva ese valor definido
        * Ej aqui el valor minimo es -1 asi que si pasa a ser -2, -3, etc se interpreta como -1 y si es mayor a 1 igualmente se vuelve 1
        */

        float clampedX = Mathf.Clamp(target.x, -1f, 1f);
        float clampedY = Mathf.Clamp(target.y, -1f, 1f);
        float clampedZ = Mathf.Clamp(target.z, -1f, 1f);

        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);

        return result;

    }


}
